// click on a ruleset name to see its source here
ruleset temperature_store {
  meta {
     provides temperatures, threshold_violations, inrange_temperatures
     shares temperatures, threshold_violations, inrange_temperatures
  }

  global {
      temperatures = function() {
          ent:temperatures.values();
      };

      threshold_violations = function() {
          ent:violations.values();
      };

      inrange_temperatures = function() {
          ent:temperatures.filter(function(v,k){
	     not ent:violations.keys().has(k)
          });
      };
  }
  
  rule collect_temperatures {
     select when wovyn new_temperature_reading

     pre {
         temperature = event:attr("temperature")
         timestamp = event:attr("timestamp")
         clear_temperatures = {}
     }

     send_directive("say", {"message": "New ruleset working!"});

     always {
         ent:temperatures := ent:temperatures.defaultsTo({}).put([timestamp], [timestamp, temperature]);
    }
  }

  rule collect_threshold_violations {
     select when wovyn threshold_violation

     pre {
         temperature = event:attr("temperature")
         timestamp = event:attr("timestamp")
         clear_violations = {}
     }

     send_directive("say", {"message": "New ruleset working!"});

     always {
         ent:violations := ent:violations.defaultsTo({}).put([timestamp], [timestamp, temperature]);
    }
  }

  rule clear_temeratures {
     select when sensor reading_reset

     always {
         clear ent:temperatures;
         clear ent:violations;
     }
  }

  rule test_temperatures_function {
     select when test temperature_function
     
     pre {
         temps = temperatures()
     }

     send_directive("say", {"message": temps});
  }

  rule test_threshold_violations_function {
     select when test thresholds_function

     pre {
          violations = threshold_violations()
     } 

     send_directive("say", {"message": violations});
  }

  rule test_inrange_function {
     select when test inrange_function

     pre {
         temps = inrange_temperatures()
     }

     send_directive("say", {"message": temps});
  }
}
