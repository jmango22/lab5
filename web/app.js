var orderedList = $('ul');
var title = $('#temp');
var homeBlock = $('#home');
var profileBlock = $('#profile');
var intervalID = [];

const urlBase = 'http://localhost:8080';
const eci = 'YEquMcX8zqsXiK2ff3pPXX';

// stop updates when editing
$("#name").focus(function() {
    stopUpdates();
});
$("#location").focus(function() {
    stopUpdates();
});
$("#number").focus(function() {
    stopUpdates();
});
$("#threshold").focus(function() {
    stopUpdates();
});

function fetchTemperatures() {
    $.ajax({
        url: urlBase + '/sky/event/' + eci +'/1556/test/temperature_function',
        dataType: 'json'
    }).done(function(response) {
        const temperatureList = response.directives[0].options.message;
        
        // Grab the temperature violations
        $.ajax({
            url: urlBase + '/sky/event/' + eci +'/1556/test/thresholds_function',
            dataType: 'json'
        }).done(function(violations) {
            var tempViolationsList = violations.directives[0].options.message;

            // format the timestaps so we can do tests later to see if they are valid
            var allTempsTimestamps = temperatureList.map($0 => $0[0]);
            var violationsTimestamps = tempViolationsList.map($0 => $0[0]);

            // Clear the ordered list (ol)
            orderedList.empty();
            // Add all the temperatures formated to the list
            for (i=0; i<temperatureList.length; i++) {
                var timestamp = temperatureList[i][0];
                var date = timestamp.substring(0, timestamp.indexOf('T'));
                var time = timestamp.substring(timestamp.indexOf('T')+1, timestamp.indexOf('Z')-4);

                if (i === 0) {
                    orderedList.append('<li><bold>Temperature Log: </bold></li>');
                }
                // temperature is a violation 
                if (violationsTimestamps.indexOf(temperatureList[i][0]) > -1 ) {
                    orderedList.append('<li><span class="violation">' + date + '  ' + time + ': ' + temperatureList[i][1] + 'F' + '</span></li>');                
                }
                // temperature is normal 
                else {
                    orderedList.append('<li>' + date + ' ' + time + ': ' + temperatureList[i][1] + 'F' + '</li>');    
                }

                // Last temperature is always the current temperature
                if (i === (temperatureList.length-1)) {
                    title.html(temperatureList[i][1] + 'F');
                }
            }
            
            console.log("Fetched temperatures!");
        }).fail(function(xhr, status, errorThrown) {
            console.log( "Error: " + errorThrown );
            console.log( "Status: " + status );
        });
    }).fail(function(xhr, status, errorThrown) {
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
    });
}

function fetchProfile() {
    $.ajax({
        url: urlBase + '/sky/event/' + eci +'/1556/sensor/profile',
        dataType: 'json'
    }).done(function(response) {
        console.log("response: " + response);
        var profile = response.directives[0].options;
        updateInputFeild('name', profile.name);
        updateInputFeild('location', profile.location);
        updateInputFeild('number', profile.contact_number);
        updateInputFeild('threshold', profile.threshold);
    }).fail(function(xhr, status, errorThrown) {
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
    });
}

function updateProfile() {
    $.ajax({
        type: "POST",
        url: urlBase + '/sky/event/' + eci +'/1556/sensor/profile_updated',
        data: {
            name: $('#name').val(),
            location: $('#location').val(),
            number: $('#number').val(),
            threshold: $('#threshold').val()
        }
      }).fail(function(xhr, status, errorThrown) {
        console.log( "Error: " + errorThrown );
        console.log( "Status: " + status );
    });
    startUpdates(fetchProfile);
}


function updateInputFeild(id, value) {
    if (!$('#'+id).is(":focus")) {
        $('#'+id).val(value);
    }
}

function handleHomeClick() {
    fetchTemperatures();
    stopUpdates();
    startUpdates(fetchTemperatures);

    changeVisibility(profileBlock, false);
    changeVisibility(homeBlock, true);
}

function handleProfileClick() {
    fetchProfile();
    stopUpdates();
    startUpdates(fetchProfile);
    
    changeVisibility(homeBlock, false);
    changeVisibility(profileBlock, true);
}

function changeVisibility(elem, visible) {
    if(visible) {
        if(elem.hasClass('hide')) {
            elem.removeClass('hide');
        }
    } else {
        if(!elem.hasClass('hide')) {
            elem.addClass('hide');
        }
    }
}

function startUpdates(fetchFunction) {
    while(intervalID.length > 0) {
        clearInterval(intervalID.pop());
    }
    intervalID.push(setInterval(fetchFunction, 3000));
}

function stopUpdates() {
    if (intervalID.length > 0) {
        id = intervalID.pop();
        clearInterval(id);
    }
}

fetchTemperatures();
fetchProfile();
startUpdates(fetchTemperatures);